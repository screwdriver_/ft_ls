/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 13:13:37 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/22 21:29:12 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LS_H
# define LS_H

# include <dirent.h>
# include <errno.h>
# include <grp.h>
# include "libft/libft.h"
# include <pwd.h>
# include <stdlib.h>
# include <string.h>
# include <sys/syslimits.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <time.h>
# include <unistd.h>

typedef enum	e_param
{
	LONG = 'l',
	RECURSE = 'R',
	HIDDEN = 'a',
	REVERSE = 'r',
	TIME = 't',
	ONELINE = '1'
}				t_param;

typedef enum	e_flag
{
	LONG_F = 0b01,
	RECURSE_F = 0b10,
	HIDDEN_F = 0b100,
	REVERSE_F = 0b1000,
	TIME_F = 0b10000,
	ONELINE_F = 0b100000
}				t_flag;

typedef char	t_flags;

typedef struct	s_file
{
	const char	*path;
	const char	*name;
	struct stat	st;
	char		type;
	t_bool		points_to_dir;
}				t_file;

typedef enum	e_file_type
{
	BLOCK_SPECIAL = 'b',
	CHARACTER_SPECIAL = 'c',
	DIRECTORY = 'd',
	LINK = 'l',
	SOCKET = 's',
	FIFO = 'p',
	REGULAR = '-'
}				t_file_type;

t_bool			is_param(const char *str);
void			param_to_flag(const char param, t_flags *flags);

void			del_lst(void *content, size_t content_size);

void			print_error(const int stop);
void			print_file_error(const int stop, const char *file);
void			invalid_param(const char param);

t_file			*new_file(const char *path);
void			del_file(void *content, size_t content_size);
void			free_file(const t_file *file);

size_t			get_links(const t_file *file);
const char		*get_owner(const t_file *file);
const char		*get_group(const t_file *file);
size_t			get_size(const t_file *file);
const char		*get_date(const t_file *file);

t_file			*follow_link(const t_file *file);
t_bool			points_to_dir(const t_file *file);

t_list			*iterate_dir(const t_file *file, const t_flags flags);

void			list_files(t_file *file, const t_flags flags);
void			list(t_list *file, const t_flags flags,
	const int param_files_count);

void			list_swap(t_list *l1, t_list *l2);
void			sort(t_list *lst, const t_flags flags);
void			sort_files(t_list *files, const t_flags flags);

void			print_path(const char *path, const t_bool line_feed,
	const t_bool current);
void			print_blocks(const t_list *files);
void			print_entry(const t_file *file, const t_flags flags);
void			print_entries(const t_list *files, const t_flags flags);

void			simple_print(const t_list *files, const t_flags flags);
void			print_files_list(const t_list *files, const t_flags flags,
	const t_bool path);

size_t			sizelen(const size_t size);

void			align(size_t len, const size_t max);

void			print_perms(const t_file *file);
void			print_links(const t_file *file, const size_t max);
void			print_links(const t_file *file, const size_t max);
void			print_owner(const t_file *file, const size_t max);
void			print_owner(const t_file *file, const size_t max);
void			print_group(const t_file *file, const size_t max);
void			print_size(const t_file *file, const size_t max);
void			print_date(const t_file *file);
void			print_file(const t_file *file, const t_bool path);
void			print_link(const t_file *file);

#endif
