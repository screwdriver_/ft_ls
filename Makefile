# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: llenotre <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/29 14:13:49 by llenotre          #+#    #+#              #
#    Updated: 2019/01/22 20:52:07 by llenotre         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ls
CC = gcc
CFLAGS = -Wall -Wextra -Werror -g3

SRC =	align.c\
		error.c\
		file.c\
		file_info.c\
		iterate.c\
		link.c\
		list.c\
		list_swap.c\
		main.c\
		param.c\
		print.c\
		print_date.c\
		print_entry.c\
		print_file.c\
		print_group.c\
		print_link.c\
		print_links.c\
		print_owner.c\
		print_perms.c\
		print_size.c\
		sort.c
HDR =	ls.h

OBJ = $(SRC:.c=.o)

LIBFT = libft/libft.a

all: $(NAME)

$(NAME): $(LIBFT) $(OBJ)
	$(CC) $(CFLAGS) -o $(NAME) $(OBJ) $(LIBFT)

$(LIBFT):
	make -C libft/

%.o: %.c $(HDR)
	@$(CC) $(CFLAGS) -o $@ -c $<

tags:
	ctags * libft/*

clean:
	rm -f $(OBJ)
	make clean -C libft/
	rm -f tags

fclean: clean
	rm -f $(NAME)
	make fclean -C libft/

re: fclean all

.PHONY: all tags clean fclean re
