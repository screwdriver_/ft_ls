/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 13:58:42 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/22 16:09:21 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

static char			get_file_type(const t_file *file)
{
	mode_t mode;

	if (!file)
	{
		errno = ENOENT;
		return (REGULAR);
	}
	mode = ((file->st.st_mode) & S_IFMT);
	if (mode == S_IFBLK)
		return (BLOCK_SPECIAL);
	else if (mode == S_IFCHR)
		return (CHARACTER_SPECIAL);
	else if (mode == S_IFDIR)
		return (DIRECTORY);
	else if (mode == S_IFLNK)
		return (LINK);
	else if (mode == S_IFSOCK)
		return (SOCKET);
	else if (mode == S_IFIFO)
		return (FIFO);
	return (REGULAR);
}

t_file				*new_file(const char *path)
{
	t_file	*file;

	errno = 0;
	if (!path)
		return (NULL);
	if (!(file = (t_file*)malloc(sizeof(t_file))))
		return (NULL);
	file->path = ft_strdup(path);
	file->name = ft_get_filename(path);
	if (lstat(path, &(file->st)) < 0)
	{
		free_file(file);
		return (NULL);
	}
	else
		file->type = get_file_type(file);
	if (!(file->path) || !(file->name))
	{
		free_file(file);
		return (NULL);
	}
	file->points_to_dir = points_to_dir(file);
	return (file);
}

void				del_file(void *content, size_t content_size)
{
	(void)content_size;
	free_file((t_file*)content);
}

void				free_file(const t_file *file)
{
	if (!file)
		return ;
	free((void*)file->path);
	free((void*)file->name);
	free((void*)file);
}
