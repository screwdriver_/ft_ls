/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_links.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/16 16:30:51 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/16 16:30:52 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	print_links(const t_file *file, const size_t max)
{
	align(ft_intlen(get_links(file)), max);
	ft_putnbr(get_links(file));
	ft_putchar(' ');
}
