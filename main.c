/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 13:17:09 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/22 21:28:09 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

static t_flags	get_flags(int argc, char **argv)
{
	t_flags	flags;
	size_t	i;

	flags = 0;
	if (argc == 0 || !is_param(*argv))
		return (flags);
	i = 1;
	while ((*argv)[i])
		param_to_flag((*argv)[i++], &flags);
	return (flags | get_flags(argc - 1, argv + 1));
}

static t_list	*get_files(int argc, char **argv, int *param_files_count)
{
	int		i;
	t_list	*lst;

	while (argc > 0 && is_param(*argv))
	{
		--argc;
		++argv;
	}
	if (ft_strequ(*argv, "--"))
	{
		--argc;
		++argv;
	}
	lst = NULL;
	*param_files_count = argc;
	if (argc == 0)
		ft_lstadd(&lst, ft_lstnew(".", 2));
	i = 0;
	while (i < argc)
	{
		ft_lstadd(&lst, ft_lstnew(argv[i], ft_strlen(argv[i]) + 1));
		++i;
	}
	return (lst);
}

void			del_lst(void *content, size_t content_size)
{
	(void)content_size;
	free(content);
}

int				main(int argc, char **argv)
{
	t_flags	flags;
	int		param_files_count;
	t_list	*files;

	--argc;
	++argv;
	flags = get_flags(argc, argv);
	files = get_files(argc, argv, &param_files_count);
	sort(files, flags);
	list(files, flags, param_files_count);
	ft_lstdel(&files, del_lst);
}
