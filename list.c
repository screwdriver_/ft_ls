/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 14:02:33 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/22 21:44:09 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

static void		recursive_list(t_list *files, const char *parent,
	const t_flags flags)
{
	const t_list	*l;
	t_file			*file;
	const char		*path;

	if (!files || !parent)
		return ;
	l = files;
	while (l)
	{
		file = (t_file*)l->content;
		if (file->type == DIRECTORY
			&& !ft_strequ(file->name, ".") && !ft_strequ(file->name, ".."))
		{
			path = ft_path_merge(parent, file->name);
			print_path(path, TRUE, FALSE);
			free((void*)path);
			list_files(file, flags);
		}
		l = l->next;
	}
}

void			list_files(t_file *file, const t_flags flags)
{
	t_list	*l;
	t_file	*f;

	if (!file)
		return ;
	f = (!(flags & LONG_F) ? follow_link(file) : file);
	if (!f)
		f = file;
	if (f && f->type == DIRECTORY)
	{
		l = iterate_dir(f, flags);
		if (errno)
			print_error(FALSE);
		if (l && flags & LONG_F)
			print_blocks(l);
		sort_files(l, flags);
		print_entries(l, flags);
		if (flags & RECURSE_F)
			recursive_list(l, file->path, flags);
		ft_lstdel(&l, del_file);
	}
	else
		print_entry(file, flags);
	if (f && f != file)
		free_file((void*)f);
}

static void		list_dirs(t_list *files, const t_flags flags,
	const int param_files_count, t_bool first)
{
	t_list	*l;
	t_file	*file;

	l = files;
	while (l)
	{
		if (l->content)
		{
			if (!(file = new_file((const char*)l->content)) || errno)
			{
				print_file_error(FALSE, l->content);
				ft_memdel(&(l->content));
			}
			if (file->points_to_dir)
			{
				if ((param_files_count > 1) || (flags & RECURSE_F))
					print_path(file->path, !first, (param_files_count > 1));
				list_files(file, flags);
				first = FALSE;
			}
			free_file(file);
		}
		l = l->next;
	}
}

void			list(t_list *files, const t_flags flags,
	const int param_files_count)
{
	t_bool	first;
	t_list	*l;
	t_file	*file;

	first = TRUE;
	l = files;
	while (l)
	{
		file = new_file((const char*)l->content);
		if (errno || !file)
		{
			print_file_error(FALSE, l->content);
			ft_memdel(&(l->content));
		}
		if (file && !file->points_to_dir)
		{
			list_files(file, flags);
			first = FALSE;
		}
		free_file(file);
		l = l->next;
	}
	list_dirs(files, flags, param_files_count, first);
}
