/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/12 14:18:30 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/17 14:46:41 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void		simple_print(const t_list *files, const t_flags flags)
{
	t_bool			oneline;
	const t_list	*l;
	const t_file	*file;

	oneline = (flags & ONELINE_F);
	l = files;
	while (l)
	{
		file = (const t_file*)l->content;
		print_file(file, FALSE);
		ft_putchar((l->next && !oneline ? ' ' : '\n'));
		l = l->next;
	}
}

static void	get_maxs(const t_list *files, size_t *maxs)
{
	const t_list	*l;
	const t_file	*file;
	size_t			i;

	l = files;
	while (l)
	{
		file = (const t_file*)l->content;
		if ((i = ft_intlen(get_links(file))) > maxs[0])
			maxs[0] = i;
		if ((i = ft_strlen(get_owner(file))) > maxs[1])
			maxs[1] = i;
		if ((i = ft_strlen(get_group(file))) > maxs[2])
			maxs[2] = i;
		if ((i = sizelen(get_size(file))) > maxs[3])
			maxs[3] = i;
		l = l->next;
	}
}

void		print_files_list(const t_list *files, const t_flags flags,
	const t_bool path)
{
	size_t			maxs[4];
	const t_list	*l;
	const t_file	*file;

	(void)flags;
	if (!files)
		return ;
	ft_bzero(maxs, sizeof(maxs));
	get_maxs(files, maxs);
	l = files;
	while (l)
	{
		file = ((const t_file*)l->content);
		print_perms(file);
		print_links(file, maxs[0]);
		print_owner(file, maxs[1]);
		print_group(file, maxs[2]);
		print_size(file, maxs[3]);
		print_date(file);
		print_file(file, path);
		if (file->type == LINK)
			print_link(file);
		ft_putchar('\n');
		l = l->next;
	}
}
