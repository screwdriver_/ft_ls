/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_link.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/16 16:30:48 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/22 16:36:11 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	print_link(const t_file *file)
{
	char buffer[PATH_MAX];

	ft_bzero(buffer, PATH_MAX);
	if (readlink(file->path, buffer, PATH_MAX) < 0)
		print_error(1);
	ft_putstr(" -> ");
	ft_putstr(buffer);
}
